import 'dart:async';

import 'package:clock_app/constants/theme_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import 'clockview.dart';

class ClockPage extends StatefulWidget {
  @override
  _ClockPageState createState() => _ClockPageState();
}

class _ClockPageState extends State<ClockPage> {
  CalendarDataSource _dataSource;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text("calander",
              style: TextStyle(
                color: Colors.black,
              )),
          backgroundColor: Colors.white,
        ),
        body: SfCalendar(
          backgroundColor: Colors.white,
          view: CalendarView.month,
          showNavigationArrow: true,
          cellEndPadding: 10,
          initialSelectedDate: DateTime.now(),
          todayHighlightColor: Color(0xFF2D2F41),
          showDatePickerButton: true,
          allowViewNavigation: true,
          allowedViews: <CalendarView>[
            CalendarView.day,
            CalendarView.week,
            CalendarView.workWeek,
            CalendarView.month,
            CalendarView.schedule
          ],
          selectionDecoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(color: Color(0xFF2D2F41), width: 2),
            borderRadius: const BorderRadius.all(Radius.circular(4)),
            shape: BoxShape.rectangle,
          ),
          dataSource: _dataSource,
          // MeetingDataSource(_getDataSource()),
          monthViewSettings: const MonthViewSettings(
            showAgenda: true,
            dayFormat: 'EEE',
            agendaViewHeight: 250,
            agendaStyle: AgendaStyle(
              appointmentTextStyle: TextStyle(
                  fontSize: 14,
                  fontStyle: FontStyle.italic,
                  color: Color(0xFF000000)),
              dateTextStyle: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 12,
                  fontWeight: FontWeight.w300,
                  color: Colors.black),
              dayTextStyle: TextStyle(
                  fontStyle: FontStyle.normal,
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  color: Colors.black),
            ),
          ),
        ));
  }
}

class Meeting {
  Meeting(this.eventName, this.from, this.to, this.background, this.isAllDay);

  String eventName;

  DateTime from;

  DateTime to;

  Color background;

  bool isAllDay;
}
